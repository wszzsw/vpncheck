﻿using Dapper;
using Dapper.FastCrud;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WindowsFirewallHelper;
using WindowsFirewallHelper.Addresses;

namespace VPNCheck
{
    class Program
    {
        public static List<string> AlreadyChecked;

        static void Main(string[] args)
        {

            
            try
            {
                var logFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "VPNCheck.log");

                Log.Logger = new LoggerConfiguration()
                             .WriteTo.Async(log => log.File(logFile))
                             .WriteTo.Async(console => console.Console(outputTemplate: "[{Level:u3}] |{SourceContext}| {Message}{NewLine}{Exception}"))
                             .MinimumLevel.Verbose()
                             .CreateLogger();

                var Logger = Log.ForContext(Constants.SourceContextPropertyName, "Program");


                Database.Init();

                AlreadyChecked = new List<string>();

                Logger.Information("New TCP Connections: ");

                while (true)
                {
                    Check.CheckIP();
                    Task.Delay(2000).Wait();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public static void AddIP(string rule, string ip)
        {
            var ruleName = FirewallManager.Instance.Rules.Where(o => o.Direction == FirewallDirection.Inbound && o.Name.Equals(rule)).FirstOrDefault();
            if (ruleName != null)
            {
                List<IAddress> ips = ruleName.RemoteAddresses.ToList<IAddress>();
                ips.Add(SingleIP.Parse(ip));
                ruleName.RemoteAddresses = ips.ToArray<IAddress>();
            }
        }

        private static readonly ILogger MainLogger = Log.ForContext(Constants.SourceContextPropertyName, nameof(Program));
        public static class Database
        {
            private static string s_connection;
            public static void Init()
            {
                try
                {
                    MainLogger.Information($"Initializing Database..");

                    s_connection = $"SslMode=none;Server=35.212.112.202;Port=3306;Database=iplist2;Uid=Chai;Pwd=admin1;Pooling=true;";
                    OrmConfiguration.DefaultDialect = SqlDialect.MySql;

                    using (var con = Open())
                    {
                        if (con.QueryFirstOrDefaultAsync($"SHOW DATABASES LIKE \"iplist\"") == null)
                        {
                            MainLogger.Error($"Database 'iplist' not found!");
                            Task.Delay(2000).Wait();
                            Environment.Exit(-1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw;
                }
            }

            public static IDbConnection Open()
            {
                IDbConnection connection;

                connection = new MySqlConnection(s_connection);

                connection.Open();
                return connection;
            }
        }
    }
}
