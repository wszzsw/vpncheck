﻿using Dapper;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using WindowsFirewallHelper;

namespace VPNCheck
{
    public class Check
    {
        private static readonly ILogger Logger = Log.ForContext(Constants.SourceContextPropertyName, nameof(Check));
        private static List<TcpConnectionInformation> cIPs = null;
        private static List<TcpConnectionInformation> CIPs;
        
        public static List<TcpConnectionInformation> CheckedIPs
        {
            get => cIPs;
            set => cIPs = value;
        }

        public static List<TcpConnectionInformation> CheckIPs
        {
            get => CIPs;
            set => CIPs = value;
        }
        public static async Task WriteConfig(string msg)
        {
            using (StreamWriter sw = new StreamWriter("count.txt"))
            {
                sw.Write(msg);
                sw.Close();
            }
        }
        public static string ReadConfig()
        {
            using (StreamReader sr = new StreamReader("count.txt"))
            {
                string content = sr.ReadLine();
                sr.Close();
                return content;
            }

        }
        public static Boolean DoesRuleExist(string ruleName)
        {
            var rName = FirewallManager.Instance.Rules.FirstOrDefault(o => o.Direction == FirewallDirection.Inbound && o.Name.Equals(ruleName));

            if (rName != null) return true;
            return false;
        }
        public static Boolean isRuleFull(string ruleName)
        {
            var rName = FirewallManager.Instance.Rules.FirstOrDefault(o => o.Direction == FirewallDirection.Inbound && o.Name.Equals(ruleName));
            if (rName.RemoteAddresses.Length >= 1000) return true;

            return false;
        }

        public static async Task CheckIP()
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] connetions = properties.GetActiveTcpConnections();
            List<TcpConnectionInformation> cons = connetions.AsList();
            try
            {
                foreach (var c in connetions)
                {
                    if (Program.AlreadyChecked.Any(x => x == c.RemoteEndPoint.Address.ToString()))
                    {
                        continue;
                    }
                    
                    if (c.State != TcpState.Closed)
                    {
                        string[] IP = c.RemoteEndPoint.ToString().Split(":");

                        if (IP[0] != "127.0.0.1" && IP[0] != "[" && IP[0] != "151.80.44.33" && IP[0] != "10.150.0.2" && IP[1] != "443" && IP[1] != "80")
                        {
                            Program.AlreadyChecked.Add(c.RemoteEndPoint.Address.ToString());

                            if (!await DataAccess.CheckBlacklist(IP[0]))
                            {
                                if (!await DataAccess.CheckWhitelist(IP[0]))
                                {
                                    using (var wc = new WebClient())
                                    {
                                        string url = "https://kirika.moe/ipapi.php?ip=";
                                        Stream stream = wc.OpenRead(url + IP[0]);
                                        StreamReader reader = new StreamReader(stream);
                                        string isProxy = await reader.ReadToEndAsync();
                                        bool _isproxy = true;

                                        if (isProxy.ToLower() == "n")
                                            _isproxy = false;

                                        Logger.Information("IP {0} Port {1} isProxy {2}", IP[0], IP[1], _isproxy);

                                        if (isProxy.ToLower() != "n")
                                        {
                                            string rulePrefix = "IPBan_";
                                            int ruleNum = Int32.Parse(ReadConfig());


                                            if (DoesRuleExist(rulePrefix + ruleNum))
                                            {
                                                if (isRuleFull(rulePrefix + ruleNum))
                                                {
                                                    //.Information(rulePrefix + ruleNum);
                                                    ruleNum++;
                                                    WriteConfig(ruleNum.ToString());
                                                    Process.Start("netsh.exe", $"advfirewall firewall add rule name=\"{rulePrefix + ruleNum}\" dir=in remoteip={IP[0]} interfac=any action=block");
                                                }
                                                else if (!isRuleFull(rulePrefix + ruleNum))
                                                {
                                                    Program.AddIP(rulePrefix + ruleNum, IP[0]);
                                                    await DataAccess.AddBlacklist(IP[0]);
                                                }
                                            }
                                            else if (!DoesRuleExist(rulePrefix + ruleNum))
                                            {
                                                Process.Start("netsh.exe", $"advfirewall firewall add rule name=\"{rulePrefix + ruleNum}\" dir=in remoteip={IP[0]} interfac=any action=block");
                                            }


                                            
                                            //Process.Start("netsh.exe", $"advfirewall firewall add rule name={IP[0]} dir=in interfac=any action=block remoteip={IP[0]}");
                                            return;
                                        }
                                        else
                                        {
                                            await DataAccess.AddWhitelist(IP[0]);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
            }
        }
    }


}
