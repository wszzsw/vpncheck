﻿using Dapper.FastCrud;
using System.Linq;
using System.Threading.Tasks;
using VPNCheck.Models;

namespace VPNCheck
{
    public static class DataAccess
    {
        public static async Task<bool> CheckBlacklist(string ipaddress)
        {
            using (var db = Program.Database.Open())
            {
                var q = (await db.FindAsync<IPBlacklistDto>(state => state
                                                                     .Where($"{nameof(IPBlacklistDto.IPAddress):C} = @{nameof(ipaddress)}")
                                                                     .WithParameters(new { IPAddress = ipaddress }))).FirstOrDefault();

                if (q == null)
                    return false;
                return true;
            }
        }

        public static async Task<bool> CheckWhitelist(string ipaddress)
        {
            using (var db = Program.Database.Open())
            {
                var q = (await db.FindAsync<IPWhitelistDto>(state => state
                                                                     .Where($"{nameof(IPWhitelistDto.IPAddress):C} = @{nameof(ipaddress)}")
                                                                     .WithParameters(new { IPAddress = ipaddress }))).FirstOrDefault();

                if (q == null)
                    return false;
                return true;
            }
        }

        public static async Task AddBlacklist(string ip)
        {
            using (var db = Program.Database.Open())
            {
                await db.InsertAsync(new IPBlacklistDto
                {
                    IPAddress = ip
                });
            }
        }

        public static async Task AddWhitelist(string ip)
        {
            using (var db = Program.Database.Open())
            {
                await db.InsertAsync(new IPWhitelistDto
                {
                    IPAddress = ip
                });
            }
        }
    }
}
