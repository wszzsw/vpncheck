﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VPNCheck.Models
{
    [Table("blacklist")]
    public class IPBlacklistDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string IPAddress { get; set; }
    }

    [Table("whitelist")]
    public class IPWhitelistDto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string IPAddress { get; set; }
    }
}
